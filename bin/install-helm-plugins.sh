#!/usr/bin/env bash

# shfmt:skip

set -euo pipefail

if [[ -n "${BASH_VERSION:-}" ]] && [[ "${BASH_VERSION%%.*}" -lt 4 ]]; then
  if command -v zsh >/dev/null 2>&1; then
    echo "Bash ${BASH_VERSION} not supported, executing with zsh instead" >&2
    exec zsh "$0" "$@"
  else
    echo "You need zsh or bash >= v4 to run this script, currently running ${BASH_VERSION}" >&2
    exit 1
  fi
fi

typeset -A versions urls
versions=(
  ["diff"]="v3.8.1"
)
urls=(
  ["diff"]="https://github.com/databus23/helm-diff"
)

# shellcheck disable=SC2296
for plugin in ${BASH_VERSION+${!versions[@]}} ${ZSH_VERSION+${(@k)versions}}; do
  if helm plugin list | grep -E "^${plugin}\\s+" >/dev/null 2>&1; then
    helm plugin remove "${plugin}"
  fi

  helm plugin install --version "${versions[${plugin}]}" "${urls[${plugin}]}"
done
