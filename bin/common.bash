#!/usr/bin/env bash
# vim: ai:ts=2:sw=2:et
##############################
# Common functions and variables for all k8s-workloads
# Requires the following ENV variables to be set:
#   ENV: The environment
#   CLUSTER: The GKE cluster name
#   REGION: The region for the cluster

## Colors
_NORM="\\033[0m"
_BLK="\\033[0;30m"
_BBLK="\\033[1;30m"
_RED="\\033[0;31m"
_BRED="\\033[1;31m"
_GRN="\\033[0;32m"
_BGRN="\\033[1;32m"
_YEL="\\033[0;33m"
_BYEL="\\033[1;33m"
_BLU="\\033[0;34m"
_BBLU="\\033[1;34m"
_MAG="\\033[0;35m"
_BMAG="\\033[1;35m"
_CYN="\\033[0;36m"
_BCYN="\\033[1;36m"
_WHT="\\033[0;37m"
_BWHT="\\033[1;37m"

export _NORM _BLK _RED _GRN _YEL _BLU _MAG _CYN \
  _WHT _BBLK _BRED _BGRN _BYEL _BBLU _BMAG \
  _BCYN _BWHT

debug() {
  msg=$1
  color=${2:-${_NORM}}
  echo -e "${color}${msg}${_NORM}" >&2
}

set_vars_for_env() {
  env_config_files=(
    "$dir/../$ENV.yaml"
  )
  STAGE=${STAGE:-main}
  if [[ "$STAGE" != "main" ]]; then
    STAGE_SUFFIX="-$STAGE"
    env_config_files+=("$dir/../${ENV}${STAGE_SUFFIX}.yaml")
  else
    STAGE_SUFFIX=''
  fi

  CLUSTER="${CLUSTER:-$ENV-gitlab-gke}"
  REGION="${REGION:-us-east1}"
  case "$ENV" in
    gprd*)
      PROJECT=${PROJECT:-"gitlab-production"}
      GITLAB_ENDPOINT=${GITLAB_ENDPOINT:-"https://gitlab.com"}
      ;;
    gstg*)
      PROJECT=${PROJECT:-"gitlab-staging-1"}
      GITLAB_ENDPOINT=${GITLAB_ENDPOINT:-"https://staging.gitlab.com"}
      ;;
    pre*)
      PROJECT=${PROJECT:-"gitlab-pre"}
      GITLAB_ENDPOINT=${GITLAB_ENDPOINT:-"https://pre.gitlab.com"}
      ;;
    ops)
      PROJECT=${PROJECT:-"gitlab-ops"}
      GITLAB_ENDPOINT=${GITLAB_ENDPOINT:-"https://ops.gitlab.net"}
      ;;
    vault)
      PROJECT=${PROJECT:-"gitlab-vault"}
      ;;
    *) ;;
  esac
}

join_by() {
  local IFS="$1"
  shift
  echo "$*"
}

is_bin_in_path() {
  command -v "$1" &>/dev/null
}

verify_cluster() {
  case "$LOCAL_MODE" in
    minikube)
      if ! minikube status; then
        debug "You indicated the use of minikube, but we failed to get it's status properly" "${_BRED}"
        return 1
      else
        debug "Using minikube" "${_CYN}"
      fi
      ;;
    k3d)
      if ! k3d check-tools; then
        debug "You indicated the use of k3d, but we failed to get it's status properly." "${_BRED}"
        return 1
      else
        debug "Using k3d" "${_CYN}"
      fi
      ;;
    docker-desktop)
      if ! kubectx | grep -q docker-desktop; then
        debug "You are attempting to use docker-desktop but the context is not present, is it configured properly?" "${_BRED}"
        return 1
      else
        debug "Using docker-desktop" "${_CYN}"
      fi
      ;;
    *)
      if ! gcloud --project "$PROJECT" container clusters list --filter "$CLUSTER" \
        --format json | jq -er '.[].selfLink' >/dev/null; then
        debug "Unable to find cluster $CLUSTER in project $PROJECT, does it exist?" "${_BRED}"
        return 1
      fi
      ;;
  esac
}

switch_kubectx_gke() {
  local kubectx
  kubectx="gke_${PROJECT}_${REGION}_${CLUSTER}"
  debug "Switching kubectx context for GKE to $kubectx" "${_YEL}"
  if ! kubectx "$kubectx"; then
    debug "Unable to switch to context \"$kubectx\", aborting" "${_BRED}"
    return 1
  fi
}

setup_local_mode() {
  case "$LOCAL_MODE" in
    k3d)
      KUBECONFIG="$(k3d get-kubeconfig --name='k3s-default')"
      export KUBECONFIG
      ;;
    minikube | docker-desktop)
      local kubectx
      kubectx="$LOCAL_MODE"
      debug "Switching kubectx context for local mode to $kubectx" "${_YEL}"
      if ! kubectx "$kubectx"; then
        debug "Unable to switch to context \"$kubectx\", aborting" "${_BRED}"
        return 1
      fi
      ;;
    *) ;;

  esac
}

overview() {
  debug "-- Overview --" "${_CYN}"
  debug "Environment: ${_BLU}$ENV" "${_MAG}"
  debug "Region: ${_BLU}$REGION" "${_MAG}"
  debug "Project: ${_BLU}$PROJECT" "${_MAG}"
  debug "Cluster: ${_BLU}$CLUSTER" "${_MAG}"
  debug "Stage: ${_BLU}$STAGE" "${_MAG}"
  debug "Namespace: ${_BLU}$NAMESPACE" "${_MAG}"
  debug "Release Name: ${_BLU}$NAME" "${_MAG}"
  debug "Operation: ${_BLU}$ACTION" "${_MAG}"
  debug "Dry Run: ${_BLU}${dry_run:-no}" "${_MAG}"
  debug "GitLab Endpoint: ${_BLU}${GITLAB_ENDPOINT:-not set}" "${_MAG}"
  debug "Local mode: ${_BLU}${LOCAL_MODE:-no}" "${_MAG}"
  debug "Auto Deploy: ${_BLU}${AUTO_DEPLOY:-false}" "${_MAG}"
  debug "GitLab image tag: ${_BLU}${GITLAB_IMAGE_TAG:-not set}" "${_MAG}"
  debug "Skip image check: ${_BLU}${IMAGE_CHECK_OVERRIDE:-no}" "${_MAG}"
  debug "Post-deployment patch: ${_BLU}${POST_DEPLOYMENT_PATCH:-no}" "${_MAG}"
  debug "-------" "${_CYN}"
}

pre_checks() {
  debug "Running prechecks, please wait ..." "${_CYN}"
  if [[ -z $ENV ]]; then
    debug "You must provide an env" "${_BRED}"
    return 1
  fi

  if [[ -z "$PROJECT" ]]; then
    debug "There is no project defined for $ENV, define it in common/bin/common.bash" "${_BRED}"
    return 1
  fi

  if [[ -z ${CI_JOB_ID:-} ]]; then
    # pre-checks for running locally only
    if ! is_bin_in_path kubectx; then
      debug "Unable to find the binary kubectx in your path, is it installed?" "${_BRED}"
      return 1
    fi
  fi

  if ! (verify_stage &&
    verify_cluster); then
    return 1
  fi
}

usage() {
  local action_names local_mode_names
  action_names=$(join_by "|" "${actions[@]}")
  local_mode_names=$(join_by "|" "${local_modes[@]}")
  cat <<-EOF
    Usage $0 [-e env] [-s stage] [-D] <$action_names>

    DESCRIPTION
        This script is wrapper over the helm command for a consistent managing
        of charts and performing maintenance on them

    FLAGS
        -e  environment
        -D  run in Dry-run
        -s  stage
        -l  local mode type, one of <$local_mode_names>
        -t  test your local workstation for tool requirements, then exit

    ACTIONS
        install:  Install the helm chart
        list:     List all helm charts that are installed
        upgrade:  Upgrade this chart
        remove:   Remove this chart
        template: Generate kubernetes config using helm template
        config:   Display the configurations for the environment
        values:   Write values files for releases.

    EXAMPLES
            $0 -e pre upgrade
            $0 -e gstg list
            # dry-run an upgrade on the pre environment
            $0 -e pre -D upgrade
            # displays variable configurations for environment
            $0 -e pre config
            # installs the application into the canary stage for gprd
            $0 -e gprd -s cny install
            # uses local mode to install into preprod
            $0 -e pre -l docker-desktop install
            # writes helm values files to the releases directory
            $0 -e pre -e pre values

	EOF
}

array_contains() {
  local array="$1[@]"
  local seeking=$2
  local in=1
  for element in "${!array}"; do
    if [[ $element == "$seeking" ]]; then
      in=0
      break
    fi
  done
  return $in
}

get_static_ip() {
  local address

  if [[ -n "$LOCAL_MODE" ]]; then
    address='10.96.0.1'
    debug "Using $LOCAL_MODE, passing in fake IP Address: $address for $1" "${_YEL}"
    echo $address
    return
  fi

  address=$(gcloud --project "$PROJECT" compute addresses list \
    --filter="name=$1" --format json |
    jq -er '.[].address')
  if [[ -z $address ]]; then
    debug "Unable to find a static ip with name $1 project $PROJECT, aborting." "${_BRED}"
    exit 1
  fi
  echo "$address"
}

verify_stage() {
  local allowed_stages=("main" "cny")
  if ! array_contains allowed_stages "$STAGE"; then
    echo -e "${_BRED}ERROR: You must specify a stage of $(join_by , "${allowed_stages[@]}")${_NORM}"
    usage
    exit 1
  fi
}

verify_installed_secrets() {
  local items=("$@")
  debug "Validating secrets..." "${_CYN}"
  local exit_code=0
  for item in "${items[@]}"; do
    if ! kubectl get secrets -n "$NAMESPACE" "$item" >/dev/null; then
      debug "Secret: ${item} is not yet set, are secrets configured properly?" "${_BRED}"
      exit_code=1
    fi
  done

  if [[ $exit_code != "0" ]]; then
    exit $exit_code
  fi
}

verify_workstation() {
  exit_code=0
  if ! is_bin_in_path gcloud; then
    debug "☠ gcloud: install via: https://cloud.google.com/sdk/docs/quickstarts" "${_BRED}"
    exit_code=1
  else
    debug "✔ gcloud" "${_GRN}"
  fi

  if ! is_bin_in_path kubectl; then
    debug "☠ kubectl: install via: https://kubernetes.io/docs/tasks/tools/install-kubectl/" "${_BRED}"
    exit_code=1
  else
    debug "✔ kubectl" "${_GRN}"
  fi

  if ! is_bin_in_path kubectx; then
    debug "☠ kubectx: install via: https://github.com/ahmetb/kubectx#installation" "${_BRED}"
    exit_code=1
  else
    debug "✔ kubectx" "${_GRN}"
  fi

  if ! is_bin_in_path helmfile; then
    debug "☠ helmfile: install via: https://helmfile.readthedocs.io/en/latest/#installation" "${_BRED}"
    exit_code=1
  else
    debug "✔ helmfile" "${_GRN}"
  fi

  if ! is_bin_in_path helm; then
    debug "☠ helm: install via: https://helm.sh/docs/using_helm/#installing-helm" "${_BRED}"
    exit_code=1
  else
    debug "✔ helm" "${_GRN}"
    if ! helm plugin list | grep -qe "^diff"; then
      debug "☠ helm plugin: diff: install via: https://github.com/databus23/helm-diff#install" "${_BRED}"
      exit_code=1
    else
      debug "✔ helm plugin: diff" "${_GRN}"
    fi
  fi

  if ! is_bin_in_path jq; then
    debug "☠ jq: install via: https://stedolan.github.io/jq/download/" "${_BRED}"
    exit_code=1
  else
    debug "✔ jq" "${_GRN}"
  fi

  if ! is_bin_in_path minikube; then
    debug "☠ OPTIONAL: minikube: install via: https://kubernetes.io/docs/tasks/tools/install-minikube/" "${_BRED}"
  else
    debug "✔ OPTIONAL: minikube" "${_GRN}"
  fi

  if ! is_bin_in_path k3d; then
    debug "☠ OPTIONAL: k3d: install via: https://github.com/rancher/k3d#get" "${_BRED}"
  else
    debug "✔ OPTIONAL: k3d" "${_GRN}"
  fi
  exit $exit_code
}

warn_removal() {
  debug "!! You are about to remove the $CHART chart on ${LOCAL_MODE:-$CLUSTER} in $ENV ($PROJECT) !!" "${_BRED}"
  echo -ne "press enter to continue." >&2
  read -rp ""
  debug "continuing..." "${_CYN}"
}

main() {
  ACTION="${1:-}"
  LOCAL_MODE="${LOCAL_MODE:-}"
  actions=("install" "list" "remove" "template" "upgrade" "config" "values")
  local_modes=("k3d" "minikube" "docker-desktop")

  ENV="${CI_ENVIRONMENT_NAME:-${environment:-}}"
  if [[ -z $ENV ]]; then
    usage
    exit 1
  fi

  # Sets the following variables:
  #  * PROJECT
  #  * GITLAB_ENDPOINT
  #  * CLUSTER
  #  * REGION
  #  * STAGE
  #  * STAGE_SUFFIX
  #  * env_config_files
  set_vars_for_env
  ###############################

  HELM_OPTS=(
    "--color"
  )
  HELM_OPTS_VALUES+=(
    "-f" "$dir/../values.yaml"
  )
  KUBECTL_OPTS=()
  for config_file in "${env_config_files[@]}"; do
    if [[ -r "$config_file" ]]; then
      HELM_OPTS_VALUES+=("-f" "$config_file")
    fi
  done

  if [[ -n ${dry_run:-} ]]; then
    HELM_OPTS+=('--dry-run')
    KUBECTL_OPTS+=('--dry-run')
  fi

  if [[ $ACTION == "config" ]]; then
    for var in PROJECT CLUSTER REGION; do
      if [[ -z ${!var} ]]; then
        continue
      fi
      echo "$var=${!var}"
    done
    exit 0
  fi

  if ! array_contains actions "$ACTION"; then
    debug "ERROR: You must specify one of $(join_by , "${actions[@]}") for actions" "${_BRED}"
    usage
    exit 1
  fi

  if [[ -n $LOCAL_MODE ]]; then
    if ! array_contains local_modes "$LOCAL_MODE"; then
      debug "ERROR: You must specify one of $(join_by , "${local_modes[@]}") for a local mode" "${_BRED}"
      usage
      exit 1
    fi
  fi

  if [[ -z ${CI_JOB_ID:-} ]]; then
    if ! pre_checks; then
      usage
      exit 1
    fi

    if [[ -n "${FORCE_KUBE_CONTEXT:-}" ]]; then
      echo "using context $(kubectl config current-context)"
    elif [[ -z "$LOCAL_MODE" ]]; then
      switch_kubectx_gke
    else
      setup_local_mode
    fi
  fi

  export CLUSTER REGION HELM_OPTS HELM_OPTS_VALUES KUBECTL_OPTS STAGE_SUFFIX LOCAL_MODE STAGE
}

while getopts ":e:l:Ds:t" o; do
  case "${o}" in
    e)
      environment=${OPTARG}
      ;;
    D)
      dry_run="true"
      ;;
    s)
      STAGE=${OPTARG}
      ;;
    l)
      LOCAL_MODE=${OPTARG}
      ;;
    t)
      verify_workstation
      ;;
    *) ;;

  esac
done
shift $((OPTIND - 1))

main "$@"
