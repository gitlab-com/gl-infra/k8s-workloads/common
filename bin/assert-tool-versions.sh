#!/usr/bin/env bash
set -euo pipefail

main() {
  grep -v '^#' .tool-versions |
    while read -r line; do
      tool="$(echo "${line}" | awk '{print $1}')"
      version="$(echo "${line}" | awk '{print $2}')"
      "assert_version__${tool}" "${version}" || rc=$?
      if [ "${rc:-0}" -ne 0 ]; then
        echo "Expected ${tool} v${version}, got something else"
        return 1
      fi
    done
}

assert_version__helm() {
  helm version | grep -E "Version:\"v${1}\""
}

assert_version__helmfile() {
  helmfile --version | grep -E "^helmfile version v${1}$"
}

assert_version__kubectl() {
  kubectl version --client=true --short | grep -E "^Client Version: v${1}"
}

assert_version__yq() {
  yq -V | grep -E "^yq \(.*\) version v${1}\$"
}

assert_version__glab() {
  glab version | grep -E "^glab version ${1}"
}

assert_version__vendir() {
  vendir --version | grep -E "^vendir version ${1}$"
}

assert_version__go-jsonnet() {
  jsonnet --version | grep -E "^Jsonnet commandline interpreter \\(Go implementation\\) v${1}$"
}

main
