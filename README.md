# common

Common configuration for k8s-workloads

This repository contains:
* scripts that are common to all k8s-workloads projects
* ci config templates can be included by k8s-workloads projects
* images that are used for all CI jobs in k8s-workloads
* generic Kubernetes Objects that would apply to all clusters

## Storage

:warning: **WARNING** :warning:

The following are _NOT_ allowed this repository:
* Files that create Kubernetes Objects of type `Secret`
* Files that contain secrets in plain text

## GitLab Environments Configuration

| Environment | URL |
| ----------- | --- |
| `pre`       | `https://pre.gitlab.com`     |
| `gstg`      | `https://staging.gitlab.com` |
| `gprd`      | `https://gitlab.com`         |

## GitLab CI/CD Variables Configuration

Each variable is applied to the environment defined above

| Variable      | Default                     | What it is  |
| --------      | --------                    | ------------|
| `CLUSTER`     | Set in `.setup.bash`        | Name of the cluster as configured in GKE |
| `PROJECT`     | Set in `common/common.bash` | Name of the project
| `SERVICE_KEY` | None                        | Key provided by the Service Account described below |
| `SERVICE_KEY_RO`| None                      | Key provided by the Service Account described below |

Note that the `SERVICE_KEY` is a `Protected` variable.  This is to ensure that
only protected branches in this project have access to this variable.  We
leverage this in order to prevent mistakes on work that is still in progress
from negatively impacting changes to the Production environment.

## GCP IAM Configuration

### Service Account `k8s-workloads`
1. `k8s-workloads` - Deploy user for our k8s-workloads configurations
    * This is currently created via Terraform
1. The user needs to be added to the correct IAM roles,
   `roles/container.developer` and `roles/compute.networkUser` you can use this example:
    * `gcloud projects add-iam-policy-binding gitlab-staging-1 --member 'serviceAccount:k8s-workloads@gitlab-staging-1.iam.gserviceaccount.com' --role 'roles/container.developer'`
    * `gcloud projects add-iam-policy-binding gitlab-staging-1 --member 'serviceAccount:k8s-workloads@gitlab-staging-1.iam.gserviceaccount.com' --role 'roles/compute.networkUser'`
    * To validate the above worked, run this, only the desired role should be
      printed: `gcloud projects get-iam-policy gitlab-staging-1 --flatten="bindings[].members" --format='table(bindings.role)' --filter="bindings.members:k8s-workloads@gitlab-staging-1.iam.gserviceaccount.com"`
1. A `json` formatted key needs to be created manually
1. The downloaded file is then copied into the `SERVICE_KEY` variable as type
   File
1. This variable should be marked as `Protected` for the Production Environment

### Service Account `k8s-workloads-ro`
1. `k8s-workloads-ro` - Read Only user for our k8s-workloads configurations
    * This is currently created via Terraform
1. The user needs to be added to the correct IAM roles,
   `roles/container.viewer` and `roles/compute.networkUser` you can use this example:
    * `gcloud projects add-iam-policy-binding gitlab-staging-1 --member 'serviceAccount:k8s-workloads-ro@gitlab-staging-1.iam.gserviceaccount.com' --role 'roles/container.viewer'`
    * `gcloud projects add-iam-policy-binding gitlab-staging-1 --member 'serviceAccount:k8s-workloads-ro@gitlab-staging-1.iam.gserviceaccount.com' --role 'roles/compute.networkUser'`
    * To validate the above worked, run this, only the desired role should be
      printed: `gcloud projects get-iam-policy gitlab-staging-1 --flatten="bindings[].members" --format='table(bindings.role)' --filter="bindings.members:k8s-workloads-ro@gitlab-staging-1.iam.gserviceaccount.com"`
1. A `json` formatted key needs to be created manually
1. The downloaded file is then copied into the `SERVICE_KEY_RO` variable as type
   File

### Cluster User Configuration

CLuster User configuration for `k8s-workloads` and `k8s-workloads-ro` is now
handled by `gitlab-ci-accounts` helm release under

https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/master/releases/gitlab-ci-accounts.yaml

Make sure that release has been run against your cluster in order to grant the Google Cloud Service Accounts
permissions inside your Kubernetes Cluster

## Decisions

One can read about how this repository is setup by
viewing the design document: https://about.gitlab.com/handbook/engineering/infrastructure/design/kubernetes-configuration/

## Working locally

The `./bin/k-ctl` script is used both locally and in CI to manage the chart for
different environments.

The following local tooling is supported:

* minikube - https://kubernetes.io/docs/tasks/tools/install-minikube/
* k3d - https://github.com/rancher/k3d
* docker-desktop - https://www.docker.com/products/docker-desktop

To configure locally, see the README.md in the corresponding
k8s-workloads project

# Generating a new common registry image

To generate a new image you must follow the git commit guidelines below, this
will trigger a semantic version bump which will then cause a new pipeline
that will build and tag the new image

## Git Commit Guidelines

This project uses [Semantic Versioning](https://semver.org). We use commit
messages to automatically determine the version bumps, so they should adhere to
the conventions of [Conventional Commits (v1.0.0-beta.2)](https://www.conventionalcommits.org/en/v1.0.0-beta.2/).

### TL;DR

- Commit messages starting with `fix: ` trigger a patch version bump
- Commit messages starting with `feat: ` trigger a minor version bump
- Commit messages starting with `BREAKING CHANGE: ` trigger a major version bump.

## Automatic versioning

Each push to `master` triggers a [`semantic-release`](https://semantic-release.gitbook.io/semantic-release/)
CI job that determines and pushes a new version tag (if any) based on the
last version tagged and the new commits pushed. Notice that this means that if a
Merge Request contains, for example, several `feat: ` commits, only one minor
version bump will occur on merge. If your Merge Request includes several commits
you may prefer to ignore the prefix on each individual commit and instead add
an empty commit summarizing your changes like so:

```
git commit --allow-empty -m '[BREAKING CHANGE|feat|fix]: <changelog summary message>'
```
